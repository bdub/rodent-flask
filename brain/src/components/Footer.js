import React, { Component } from "react";
import { Container, Grid, Header, List, Segment } from "semantic-ui-react";
export default class Footer extends Component {
  render() {
    return (
      <Segment inverted vertical style={{ padding: "5em 0em" }}>
        <Container>
          <Grid divided inverted stackable>
            <Grid.Row>
              <Grid.Column width={3}>
                <Header inverted as="h4" content="About" />
                <List link inverted>
                  <List.Item as="a">Contact Us</List.Item>
                </List>
              </Grid.Column>

              <Grid.Column width={7}>
                <p>2018 Geeked Out Solutions</p>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </Segment>
    );
  }
}
