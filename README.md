# Rodent Flask
Flask API with MongoDB for storage

## To start and load the api via docker
```
docker-compose up --build
```
You will be able to access the api on http://localhost:4000

# Endpoints
Below are a list of the current API endpoints.

## Register
/register [POST] - Takes a json payload like below:

```
{
    "name": "vader",
    "email": "vader@starwarz.com",
    "password": "lukesfather"
}
```

This will return a response stating the user was created.  You would then need to /auth.

## Auth
/auth [POST] - Takes a json payload like below and returns a JSON payload including the JWT token to be used with further API calls.

```
{
    "email": "vader@starwarz.com",
    "password": "lukesfather"
}
```

Response:

```
{
    "data": {
        "email": "vader@starwarz.com",
        "name": "vader",
        "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NDQ5NzE2NjgsIm5iZiI...",
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NDQ5NzE2NjgsIm5iZiI6M..."
    },
    "ok": true
}
```

## User
/user [GET] - Returns back your user information in JSON format
