from jsonschema import validate
from jsonschema.exceptions import ValidationError
from jsonschema.exceptions import SchemaError

###
# Litter Schema:
#   rack: Number,
#   tub: Number,
#   count: Number,
#   owner_id: String,
#   born_date: String
#
###

litter_schema = {
    "type": "object",
    "properties": {
        "rack": {
            "type": "number"
        },
        "tub": {
            "type": "number"
        },
        "count": {
            "type": "number"
        },
        "owner_id": {
            "type": "string"
        },
        "born_date": {
            "type": "string"
        },
        "owner_id": {
            "type": "string"
        }
    },
    "required": ["rack", "tub", "count"],
    "additionalProperties": False
}



def validate_litter(data):
    try:
        validate(data, litter_schema)
    except ValidationError as e:
        return {'ok': False, 'message': e}
    except SchemaError as e:
        return {'ok': False, 'message': e}
    return {'ok': True, 'data': data}