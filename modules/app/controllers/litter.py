import os
from flask import request, jsonify
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_required, jwt_refresh_token_required, get_jwt_identity)
from app import app, mongo, flask_bcrypt, jwt
from app.schemas import validate_user, validate_litter
import logger
import datetime
from bson.objectid import ObjectId


ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger( __name__)

@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401

def get_user_info(email):
    user_data = mongo.db.users.find_one({'email': email}, {"_id": 0})
    return user_data

@app.route('/litter', methods=['GET', 'POST', 'DELETE'])
@jwt_required
def litter():
    """ 
    Litter route to add or get litters
    """
    if request.method == 'GET':
        user_email = get_jwt_identity()['email']
        user_data = mongo.db.users.find_one({'email': user_email}, {"_id": 0})
        query = request.get_json()
        data = mongo.db.litters.find_one({"_id": ObjectId(query['id'])})
        return jsonify({'ok': True, 'data': data}), 200

    data = request.get_json()

    if request.method == 'POST':
        data = validate_litter(request.get_json())
        user_email = get_jwt_identity()['email']
        user_data = mongo.db.users.find_one({'email': user_email}, {"_id": 0})
        LOG.debug(user_data)
        if data['ok']:
            data['data']['owner_id'] = user_data['id']
            data['data']['born_date'] = datetime.datetime.today().strftime('%Y-%m-%d') # TODO setup checking to look for date or make it today.
            LOG.debug(data)
            data = data['data']
            mongo.db.litters.insert_one(data)
            return jsonify({'ok': True, 'message': 'Litter created successfully!'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400

    if request.method == 'DELETE':
        if data.get('id', None) is not None:
            user_email = get_jwt_identity()['email']
            user_data = get_user_info(user_email)
            LOG.debug(user_data['id'])
            litter_data = mongo.db.litters.find_one({'owner_id': user_data['id'], '_id': ObjectId(data['id'])})
            LOG.debug(litter_data)
            if litter_data['owner_id'] == user_data['id']:
                db_response = mongo.db.litters.delete_one(
                    {'_id': ObjectId(data['id'])})
                if db_response.deleted_count == 1:
                    response = {'ok': True, 'message': 'record deleted'}
                else:
                    response = {'ok': True, 'message': 'no record found'}
                return jsonify(response), 200
            else:
                return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400

@app.route('/list/litters', methods=['GET'])
@jwt_required
def list_litters():
    ''' route to get all the litters for a user '''
    # user = get_jwt_identity()
    user_email = get_jwt_identity()['email']
    user_data = get_user_info(user_email)
    if request.method == 'GET':
        query = request.args
        data = mongo.db.litters.find({'owner_id': user_data['id']})
        if query.get('group', None):
            return_data = {}
            for litter in data:
                try:
                    return_data[litter['status']].append(litter)
                except:
                    return_data[litter['status']] = [litter]
        else:
            return_data = list(data)
        return jsonify({'ok': True, 'data': return_data})